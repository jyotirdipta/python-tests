pipeline {
    agent {
        node {
            label 'my_local_server'
        }
    }
    stages {
        stage('Checkout project') {
            steps {
                script {
                    git branch: "master",
                        credentialsId: 'b579f713-a2de-48d8-a8cc-aabc4a393e06',
                        url: 'https://jyotirdipta@bitbucket.org/jyotirdipta/python-tests.git'
                }
            }
        }
        stage('Installing packages') {
            steps {
                script {
                    //sh 'pip install -r requirements.txt'
                    sh """
                        python3 --version
                        python3 -m venv .venv
                        . .venv/bin/activate
                        pip3 install wheel
                        pip3 install nose
                        pip3 install coverage
                        pip3 install pylint
                        pip3 install -r requirements.txt
                        pip3 list
                    """
                }
            }
        }
        /*stage('Static Code Checking') {
            steps {
                script {
                    sh 'find . -name \\*.py | xargs pylint -f parseable | tee pylint.log'
                    recordIssues(
                        tool: pyLint(pattern: 'pylint.log'),
                        unstableTotalHigh: 100,
                    )
                }
            }
        }*/
        /*stage('Code Quality Check') {
            steps {
                echo "Run pylint code style check"
                // Ignore pylint comments via "-d C"
                sh """
                    . .venv/bin/activate
                    find . -name \\*.py | xargs pylint -d C -f parseable | tee pylint.log
                """
            }
            post {
                always {
                    // Requires Warnings plugin since Violations plugin is deprecated
                    // http://wiki.jenkins-ci.org/x/G4CGAQ
                    warnings canComputeNew: false, canResolveRelativePaths: false, canRunOnFailed: true, categoriesPattern: '', defaultEncoding: '', excludePattern: '', healthy: '', includePattern: '', messagesPattern: '', parserConfigurations: [[parserName: 'PyLint', pattern: "pylint.log"]], unHealthy: ''
                }
            }
        }*/
        /*stage('Running Unit tests') {
            steps {
                script {
                    sh """
                       . .venv/bin/activate
                       pytest --with-xunit --xunit-file=pyunit.xml --cover-xml --cover-xml-file=cov.xml mytests/*.py || true
                     """
                    step([$class: 'CoberturaPublisher',
                        coberturaReportFile: "cov.xml",
                        onlyStable: false,
                        failNoReports: true,
                        failUnhealthy: false,
                        failUnstable: false,
                        autoUpdateHealth: true,
                        autoUpdateStability: true,
                        zoomCoverageChart: true,
                        maxNumberOfBuilds: 10,
                        lineCoverageTargets: '80, 80, 80',
                        conditionalCoverageTargets: '80, 80, 80',
                        classCoverageTargets: '80, 80, 80',
                        fileCoverageTargets: '80, 80, 80',
                    ])
                    junit "pyunit.xml"
                }
            }
        }*/
        stage('Unit Test') {
            steps {
                echo "Run unit test and coverage check"
                sh """
                    . .venv/bin/activate
                    nosetests -v --with-xunit --xunit-file=pyunit.xml --with-coverage --cover-xml --cover-xml-file=cov.xml --cover-package=mytests --cover-erase
                """
            }
            post {
                always {
                    echo "Archive test results"
                    junit allowEmptyResults: true, testResults: "pyunit.xml"

                    // Requires Cobertura plugin to archive and display converage report
                    // https://wiki.jenkins.io/display/JENKINS/Cobertura+Plugin
                    echo "Archive coverage report"
                    cobertura autoUpdateHealth: false, autoUpdateStability: false, coberturaReportFile: "cov.xml", failNoReports: false, failUnhealthy: false, failUnstable: false, maxNumberOfBuilds: 0, onlyStable: false, zoomCoverageChart: false
                }
            }
        }
        stage('Packaging') {
            steps {
                echo "Run packaging Python program"
                // Requires to run pyinstaller in each kind of machine

                script {
                    PACKAGE_ID="DemoTest"
                    PACKAGE_NAME = "${PACKAGE_ID}"
                    PACKAGE_PATH = "./dist/${PACKAGE_NAME}"
                }

                sh """
                    . .venv/bin/activate
                    pip3 install pyinstaller
                    pyinstaller employee/Employee.py  calculator/simplecalculator.py --onefile --name ${PACKAGE_NAME}
                    pwd && ls -ltra
                    echo "Package: ${PACKAGE_PATH}"
                    du -sh ${PACKAGE_PATH}
                    sha256sum ${PACKAGE_PATH}
                """
            }
            post {
                        success {
                            //This archiveArtifacts step archives the standalone executable file and exposes this file
                            //through the Jenkins interface.
                            archiveArtifacts "dist/${PACKAGE_NAME}"
                            sh "rm -rf build dist"
                        }
            }
        }
    }
}